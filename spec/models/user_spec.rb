require 'rails_helper'

describe User, type: :model do
  it { should define_enum_for(:city_preference).with_values(%i[paris londres bruxelles madrid]) }
end
