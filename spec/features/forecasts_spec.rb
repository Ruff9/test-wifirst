require 'rails_helper'

describe 'Forecasts', type: :feature do
  before :each do
    stub_five_days_forecast_paris
  end

  it 'can be searched by city' do
    allow(Date).to receive(:tomorrow).and_return Date.new(2019, 3, 20)

    create_user_and_log_in
    visit root_path

    fill_in :city_search_name, with: 'Paris'
    find('.city-search-submit').click

    expect(page).to have_content('0.07°')
    expect(page).to have_content('ciel dégagé')
  end

  it 'displays an error message when city is not found' do
    create_user_and_log_in
    visit root_path
    stub_error_response

    fill_in :city_search_name, with: 'nnnn'
    find('.city-search-submit').click

    expect(page).to have_content("Cette ville n'a pas été trouvée")
  end
end
