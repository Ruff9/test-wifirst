require 'rails_helper'

describe 'Users', type: :feature do
  before :each do
    stub_five_days_forecast_paris
    stub_five_days_forecast_london
  end

  it 'can see Paris forecast if no preferences are set' do
    create_user_and_log_in
    visit root_path

    expect(page).to have_content('Prévisions pour les 5 prochains jours à Paris')

    expect(page).to have_content('13.77°')
    expect(page).to have_content('ciel dégagé')
  end

  it 'can select preferences on home page' do
    create_user_and_log_in
    visit root_path

    select('Londres', from: :user_city_preference)
    select('3', from: :user_duration_preference)

    find('.preferences-submit').click

    expect(page).to have_content('Prévisions pour les 3 prochains jours à Londres')
  end
end
