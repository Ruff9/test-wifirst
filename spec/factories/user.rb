FactoryBot.define do
  sequence :email_adress do |n|
    "user#{n}@test.com"
  end

  factory :user do
    email { generate(:email_adress) }
    password { 'password' }
    password_confirmation { 'password' }
  end
end
