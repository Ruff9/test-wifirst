require 'rails_helper'

describe WeatherFormatter do
  it 'formats data for five days forecast' do
    raw_data = parsed_json_data('five_days_forecast')

    expected = {
      '19/03/2019' => [
        { hour: 18, temp: 9.11, weather: 'partiellement nuageux' },
        { hour: 21, temp: 5.45, weather: 'partiellement nuageux' }
      ],
      '20/03/2019' => [
        { hour: 0, temp: 2.62, weather: 'ciel dégagé' },
        { hour: 3, temp: 0.07, weather: 'ciel dégagé' },
        { hour: 6, temp: -0.64, weather: 'peu nuageux' },
        { hour: 9, temp: 8.96, weather: 'peu nuageux' },
        { hour: 12, temp: 13.06, weather: 'ciel dégagé' },
        { hour: 15, temp: 14.17, weather: 'ciel dégagé' },
        { hour: 18, temp: 11.3, weather: 'nuageux' },
        { hour: 21, temp: 7, weather: 'partiellement nuageux' }
      ],
      '21/03/2019' => [
        { hour: 0, temp: 4.1, weather: 'partiellement nuageux' },
        { hour: 3, temp: 1.86, weather: 'ciel dégagé' },
        { hour: 6, temp: 0.63, weather: 'ciel dégagé' },
        { hour: 9, temp: 10.8, weather: 'ciel dégagé' },
        { hour: 12, temp: 14.7, weather: 'ciel dégagé' },
        { hour: 15, temp:  15.83, weather: 'ciel dégagé' },
        { hour: 18, temp:  13.09, weather: 'ciel dégagé' },
        { hour: 21, temp:  7.78, weather: 'ciel dégagé' }
      ],
      '22/03/2019' => [
        { hour: 0, temp: 4.15, weather: 'ciel dégagé' },
        { hour: 3, temp: 2.21, weather: 'ciel dégagé' },
        { hour: 6, temp: 0.57, weather: 'ciel dégagé' },
        { hour: 9, temp: 9.61, weather: 'ciel dégagé' },
        { hour: 12, temp: 14.57, weather: 'ciel dégagé' },
        { hour: 15, temp: 16.66, weather: 'ciel dégagé' },
        { hour: 18, temp: 13.02, weather: 'ciel dégagé' },
        { hour: 21, temp: 6.36, weather: 'ciel dégagé' }
      ],
      '23/03/2019' => [
        { hour: 0, temp: 4.53, weather: 'ciel dégagé' },
        { hour: 3, temp: 4.27, weather: 'ciel dégagé' },
        { hour: 6, temp: 3.81, weather: 'partiellement nuageux' },
        { hour: 9, temp: 8.06, weather: 'légère pluie' },
        { hour: 12, temp: 13.35, weather: 'ciel dégagé' },
        { hour: 15, temp: 15.19, weather: 'ciel dégagé' },
        { hour: 18, temp: 12.71, weather: 'partiellement nuageux' },
        { hour: 21, temp: 9.79, weather: 'peu nuageux' }
      ],
      '24/03/2019' => [
        { hour: 0, temp: 8.61, weather: 'nuageux' },
        { hour: 3, temp: 6.69, weather: 'légère pluie' },
        { hour: 6, temp: 4.9, weather: 'ciel dégagé' },
        { hour: 9, temp: 9.61, weather: 'partiellement nuageux' },
        { hour: 12, temp: 12.99, weather: 'ciel dégagé' },
        { hour: 15, temp: 13.77, weather: 'ciel dégagé' }
      ]
    }

    expect(WeatherFormatter.five_days(raw_data)).to eq(expected)
  end

  it 'formats data for next day forecast' do
    raw_data = parsed_json_data('five_days_forecast')
    allow(Date).to receive(:tomorrow).and_return Date.new(2019, 3, 20)

    expected = {
      '20/03/2019' => [
        { hour: 0, temp: 2.62, weather: 'ciel dégagé' },
        { hour: 3, temp: 0.07, weather: 'ciel dégagé' },
        { hour: 6, temp: -0.64, weather: 'peu nuageux' },
        { hour: 9, temp: 8.96, weather: 'peu nuageux' },
        { hour: 12, temp: 13.06, weather: 'ciel dégagé' },
        { hour: 15, temp: 14.17, weather: 'ciel dégagé' },
        { hour: 18, temp: 11.3, weather: 'nuageux' },
        { hour: 21, temp: 7, weather: 'partiellement nuageux' }
      ]
    }

    expect(WeatherFormatter.custom_forecast(raw_data, 1)).to eq(expected)
  end

  it 'formats data for a custom duration' do
    raw_data = parsed_json_data('five_days_forecast')
    allow(Date).to receive(:tomorrow).and_return Date.new(2019, 3, 20)

    expected = {
      '20/03/2019' => [
        { hour: 0, temp: 2.62, weather: 'ciel dégagé' },
        { hour: 3, temp: 0.07, weather: 'ciel dégagé' },
        { hour: 6, temp: -0.64, weather: 'peu nuageux' },
        { hour: 9, temp: 8.96, weather: 'peu nuageux' },
        { hour: 12, temp: 13.06, weather: 'ciel dégagé' },
        { hour: 15, temp: 14.17, weather: 'ciel dégagé' },
        { hour: 18, temp: 11.3, weather: 'nuageux' },
        { hour: 21, temp: 7, weather: 'partiellement nuageux' }
      ],
      '21/03/2019' => [
        { hour: 0, temp: 4.1, weather: 'partiellement nuageux' },
        { hour: 3, temp: 1.86, weather: 'ciel dégagé' },
        { hour: 6, temp: 0.63, weather: 'ciel dégagé' },
        { hour: 9, temp: 10.8, weather: 'ciel dégagé' },
        { hour: 12, temp: 14.7, weather: 'ciel dégagé' },
        { hour: 15, temp:  15.83, weather: 'ciel dégagé' },
        { hour: 18, temp:  13.09, weather: 'ciel dégagé' },
        { hour: 21, temp:  7.78, weather: 'ciel dégagé' }
      ]
    }

    expect(WeatherFormatter.custom_forecast(raw_data, 2)).to eq(expected)
  end
end
