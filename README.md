# Test Wifirst

## Objectif

Réaliser une petite application web, essentiellement backend, permettant d’afficher une
prévision météorologique en utilisant l’API ​ Openweather ​ et de gérer des préférences
utilisateur.

## Points d’analyse

Bonne pratique Ruby et RoR, optimisation & performance du code, testing, gestion des
exceptions, mise en place d’API.

## Environnement

- Backend : Ruby + Ruby On Rails
- Frontend : Libre et minimaliste (standard front Rails en JS/Html), le minimum est d’avoir les API et le modèle de données pour les fonctionnalités.
- BDD : Libre

## Fonctionnalités attendues

- Un formulaire basique permettant de créer un compte utilisateur (sans validation) avec login / password
- Un formulaire de connexion avec API d’authentification basique par login / password.
- Une fois l’utilisateur connecté, l’accès à un formulaire permettant de rechercher une ville. La validation du formulaire devra déclencher l’affichage de la prévision météo du lendemain (en textuel, pas besoin de passer du temps sur la présentation visuelle).
- Une possibilité à l’utilisateurs de configurer des préférences d’affichage par défaut en page d’accueil (une ville et une sélection des jours préférées), stockés côté backend/DB côté backend.
- La page d’accueil affiche par défaut la météo selon les préférences de l’utilisateur si il les a défini, sinon affiche les 5 prochains jours de la météo de Paris

## Fonctionnalités optionnelles

- La possibilité d’afficher les prévisions météo détaillées pour un jour spécifique
- Calcul et affichage de la moyenne de température des 5 prochains jours (API
https://openweathermap.org/forecast5)