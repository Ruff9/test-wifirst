Rails.application.routes.draw do
  devise_for :users

  resources :forecast
  resources :user

  root 'forecast#index'
end
