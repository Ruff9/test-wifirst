class ForecastController < ApplicationController
  def index
    @city = current_user.city_preference || 'Paris'
    @duration = current_user.duration_preference || 5

    connector = ApiConnector.new(ENV['API_KEY'])
    forecast = connector.city_forecast(@city)

    @result = WeatherFormatter.custom_forecast(forecast, @duration)
  end

  def show
    @city = params['city_search']['name']
    connector = ApiConnector.new(ENV['API_KEY'])
    forecast = connector.city_forecast(@city)

    @result = WeatherFormatter.custom_forecast(forecast, 1)
  end
end
