module ApplicationHelper
  def options_for_cities
    User.city_preferences.keys.to_a.map { |city| [city.capitalize, city] }
  end

  def options_for_duration
    [['1', 1], ['2', 2], ['3', 3], ['4', 4], ['5', 5]]
  end
end
