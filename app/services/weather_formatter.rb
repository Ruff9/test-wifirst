require 'date'

module WeatherFormatter
  def self.five_days(data)
    result = []
    raw_data = data['list']

    raw_data.each do |chunk|
      timestamp = DateTime.strptime(chunk['dt'].to_s, '%s')

      result << {
        date: timestamp.strftime('%d/%m/%Y'),
        hour: timestamp.hour,
        temp: chunk['main']['temp'],
        weather: chunk['weather'][0]['description']
      }
    end

    result.group_by { |h| h.delete(:date) }
  end

  def self.custom_forecast(data, duration)
    return data if data[:error]

    all_data = five_days(data)
    dates = (Date.tomorrow...Date.tomorrow + duration).map { |date| date.strftime('%d/%m/%Y') }

    all_data.slice(*dates)
  end
end
