class ApiConnector
  BASE_URL = 'api.openweathermap.org/data/2.5'.freeze
  OPTIONS = 'units=metric&lang=fr'.freeze

  def initialize(api_key)
    @api_key = api_key
  end

  def city_forecast(city_name)
    url = "#{BASE_URL}/forecast?q=#{city_name}&#{OPTIONS}&appid=#{@api_key}"
    response = ::Curl.get(URI.parse(URI.escape(url)))

    handle_response(response)
  end

  private

  def handle_response(response)
    result = JSON.parse(response.body_str)

    return result if result['cod'] == '200'

    {
      error: {
        code: result['cod'],
        message: result['message']
      }
    }
  end
end
