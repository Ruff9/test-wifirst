class AddPreferencesToUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :city_preference, :integer
    add_column :users, :duration_preference, :integer
  end
end
